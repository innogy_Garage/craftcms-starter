const gulp = require('gulp');
const imagemin = require('gulp-imagemin');
const gulpIf = require('gulp-if');
const watch = require('gulp-watch');
const { isProd } = require('./server');

const config = {
	images: './src/static/img/**/*.{png,jpg,svg,gif}',
	fonts: './src/static/fonts/**/*.{ttf,otf,woff,woff2,eot}',
	videos: './src/static/videos/**/*.{mp4,mov,ogg}',
	dest: './assets',
	srcOptions: {
		base: './src/static',
	},
};

/**
 * Copy static
 */
gulp.task('copy:static', () =>
	gulp
		.src([config.fonts, config.videos], config.srcOptions)
		.pipe(gulp.dest(config.dest)),
);

/**
 * Copy images
 */
gulp.task('copy:images', () =>
	gulp
		.src([config.images], config.srcOptions)
		.pipe(gulpIf(isProd, imagemin()))
		.pipe(gulp.dest(config.dest)),
);

/**
 * Watch images
 */
gulp.task('copy:watch:images', callback =>
	watch(config.images, () => gulp.start('copy:images', callback)),
);

/**
 * Watch static
 */
gulp.task('copy:watch:static', callback =>
	watch([config.fonts, config.videos], () =>
		gulp.start('copy:static', callback),
	),
);

/**
 * Copy
 */
gulp.task('copy', ['copy:static', 'copy:images']);
