const gulp = require('gulp');
const sass = require('gulp-sass');
const postcss = require('gulp-postcss');
const sourcemaps = require('gulp-sourcemaps');
const stylelint = require('gulp-stylelint');
const watch = require('gulp-watch');
const gulpIf = require('gulp-if');
const { stream, isProd } = require('./server');

/**
 * Config
 */
const config = {
	src: './src/static/scss/style.scss',
	components: './src/templates/**/*.scss',
	static: './src/static/**/*.scss',
	dest: './assets/css',
	postCSSPresetEnv: {
		stage: 2,
		features: {
			'custom-media-queries': true,
		},
	},
	postCSS: function() {
		return [
			require('postcss-preset-env')(this.postCSSPresetEnv),
			require('cssnano')(),
		];
	},
	stylelint: {
		failAfterError: true,
		reporters: [
			{
				formatter: 'string',
				console: true,
			},
		],
	},
};

/**
 * Compile with sourcemaps and stream
 */
gulp.task('css', () =>
	gulp
		.src(config.src)
		.pipe(gulpIf(!isProd, sourcemaps.init()))
		.pipe(sass().on('error', sass.logError))
		.pipe(postcss(config.postCSS()))
		.pipe(gulpIf(!isProd, sourcemaps.write('.')))
		.pipe(gulp.dest(config.dest))
		.pipe(gulpIf(!isProd, stream())),
);

/**
 * Watch
 */
gulp.task('css:watch', callback =>
	watch([config.components, config.static], () =>
		gulp.start(['css'], callback),
	),
);

/**
 * Lint
 */
gulp.task('css:lint', () =>
	gulp
		.src([config.components, config.static])
		.pipe(stylelint(config.stylelint)),
);

module.exports.cssConfig = config;
