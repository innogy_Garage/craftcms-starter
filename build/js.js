const gulp = require('gulp');
const webpack = require('webpack-stream');
const log = require('fancy-log');
const eslint = require('gulp-eslint');
const { reload, isProd } = require('./server');
let firstCompileCompleted = false;
let callbackInvoked = false;

/**
 * Config
 */
const config = {
	src: ['./src/static/js/main.js', './src/static/js/polyfill.js'],
	all: ['./src/**/*.js', './build/**/*.js', './gulpfile.js'],
	dest: './assets/js',
	webpack: require('../webpack.config'),
};

/**
 * Handle bundle change event
 */
const onBundleChange = err => {
	if (err) {
		log.error(err);
	}

	reload();
	firstCompileCompleted = true;
};

/**
 * Handle bundle data event
 * @param {Function} callback
 */
const onData = callback => () => {
	if (firstCompileCompleted && !callbackInvoked && !isProd) {
		callback();
		callbackInvoked = true;
	}
};

/**
 * Compile js
 */
gulp.task('js', callback =>
	gulp
		.src(config.src)
		.pipe(webpack(config.webpack, null, onBundleChange))
		.pipe(gulp.dest(config.dest))
		.on('data', onData(callback)),
);

/**
 * Lint js
 */
gulp.task('js:lint', () =>
	gulp
		.src(config.all)
		.pipe(eslint())
		.pipe(eslint.format())
		.pipe(eslint.failAfterError()),
);
