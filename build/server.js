const gulp = require('gulp');
const browserSync = require('browser-sync').create();

/**
 * Config
 */
const config = {
	server: {
		proxy: 'http://localhost',
		port: '5000',
		injectChanges: true,
		open: false,
		files: './src/templates/**/*.twig',
	},
};

/**
 * Server
 */
gulp.task('server', () => {
	browserSync.init(config.server);
});

module.exports.isProd = process.env.NODE_ENV === 'production';
module.exports.stream = browserSync.stream;
module.exports.reload = browserSync.reload;
