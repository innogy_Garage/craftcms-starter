const gulp = require('gulp');
const sequence = require('run-sequence');
const del = require('del');
const { isProd } = require('./build/server');

/**
 * Import all tasks
 */
require('./build/css');
require('./build/js');
require('./build/copy');

const getTasks = () => {
	const buildTasks = [['clean'], ['css', 'js', 'copy']];
	const devTasks = [
		'css:watch',
		'copy:watch:images',
		'copy:watch:static',
		'server',
	];

	return isProd ? buildTasks : [...buildTasks, devTasks];
};

/**
 * Clean assets
 */
gulp.task('clean', callback => {
	del('./assets/*');
	callback();
});

/**
 * Build
 */
gulp.task('build', () => sequence(...getTasks()));

/**
 * Linters
 */
gulp.task('lint', ['css:lint', 'js:lint']);
