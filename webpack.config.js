/**
 * Webpack Config
 */
const env = process.env.NODE_ENV;
const isDev = process.env.NODE_ENV === 'development';

module.exports = {
	devtool: isDev ? 'cheap-eval-source-map' : 'source-map',
	mode: env,
	watch: isDev,
	output: {
		filename: '[name].bundle.js',
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				loader: 'babel-loader',
			},
		],
	},
};
