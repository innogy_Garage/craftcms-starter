# CraftCMS 3 Starter

This repository contains a starter for a CraftCMS 3 website or web application. Before you can start development there are a couple of dependencies you need to install:

**Docker**
We're using Docker to contain our development environment so that we don't have to install and run things like a webserver and PHP on our local machine. All you need to do to get started is create an account, [download](https://www.docker.com/get-started) and install the client and login into the client.

**NodeJS**
We're using Node with Gulp to run our front-end builds like SCSS to CSS and JavaScript ESNext to ES5 compilation. Instead of using the installer on the Node website I recommend installing [NVM](https://github.com/creationix/nvm#installation) (Node Version Manager). This CLI tool helps you manage different versions of Node and makes it super easy to update or switch to an older version when needed. Simply follow the installation instructions on the Git page and run `nvm install --lts` to install the long term support version.

## Development

In order to start development you have to run both Docker and Gulp.

To run Docker execute the following command:

```sh
$ docker-compose up -d
```

This will run all our Docker containers in detached mode (in the background). If you want to stop the containers you can run

```sh
$ docker-compose down
```

Before you can run Gulp you need to make sure all node dependencies are installed:

```sh
$ npm install
```

After all dependencies have been installed you can run the following command to start the development build:

```sh
$ npm run dev
```

In case you prefer to do test driven development, you can run jest in watch mode

```sh
$ npm run dev:test
```

## Production

If you want to deploy a new build of the application you can use the command

```sh
$ npm run prod
```

This will run all build tasks without the server and watchers. The production build also includes minification, image compression and advanced sourcemaps.

## Tests & Linters

The repository is configured with a pre-commit git hook that lints all JavaScript and SCSS files and runs all JavaScript tests before the commit is being stored. If any of the linters of tests fail, the commit will be canceled. This feature is configured to ensure you keep a clean and working codebase. If you want to run the linter or tests manually you can use:

```sh
$ npm run lint
$ npm run test
```
