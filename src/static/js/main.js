import '@babel/polyfill';
import * as components from '../../templates/components/**/!(*.test).js';
import loadComponents from './componentLoader';
loadComponents(components);
