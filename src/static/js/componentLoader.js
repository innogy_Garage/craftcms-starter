/**
 * Get component name
 * @param {[string, Object]} param0
 */
const getName = ([id, Component]) => [id.split('$').reverse()[0], Component];

/**
 * Get DOM element
 * @param {[string, Object]} param0
 */
const getElement = ([name, Component]) => [
	document.querySelector(`[data-component=${name}]`),
	Component,
	name,
];

/**
 * Filter loadable components
 * @param {[HTMLElement, Object, string]} param0
 */
const filterLoadables = ([element, Component, name]) => {
	if (!Component.create) {
		throw new Error(`
			Component ${name} is missing a static 'create' method \n
			and cannot be initialized.
		`);
	}
	return element;
};
/**
 * Create a new component instance
 * @param {[HTMLElement, Object]} param0
 */
const createInstance = ([element, Component]) => Component.create(element);

/**
 * Load components from glob
 * @param {{ string: Objecvt}} components
 */
export default function loadComponents(components) {
	return Object.entries(components)
		.map(getName)
		.map(getElement)
		.filter(filterLoadables)
		.map(createInstance);
}
