import Navigation from './Navigation';

describe('Navigation', () => {
	describe('static create', () => {
		it('should return a new instance', () => {
			const instance = Navigation.create('test');
			expect(instance.constructor.name).toEqual(Navigation.name);
		});
	});
});
