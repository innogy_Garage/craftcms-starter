export default class Navigation {
	/**
	 * Create
	 * Static method used to create a new instance
	 * Extract DOM interaction into this method
	 */
	static create(element) {
		return new Navigation(element);
	}

	constructor(element) {
		this._element = element;
	}
}
